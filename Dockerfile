FROM omarsmak/ontario-mongo-wrapper-base-image

COPY data/. /tmp

ENTRYPOINT ["/app/run.sh"]

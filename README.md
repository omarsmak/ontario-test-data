#Ontario Test Data Image

This image is a proof concept of Container based for Ontario. It uses this [image](https://bitbucket.org/omarsmak/ontario-mongo-wrapper-base-image) as a base image.


##Usage
Once you clone this repo, on the main directory, build the image using this command:
`docker build -t ontario-test-data --rm .`

And then run the image using this command:
`docker run -i -t --rm -p 27001:27001 ontario-test-data`